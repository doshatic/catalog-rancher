# ONLYOFFICE

Simple helm chart for running onlyoffice's documentserver container.

Chart based on a copy of the [COLLABORA code helm
chart](https://github.com/helm/charts/tree/master/stable/collabora-code)

Parameters:

**NOTE:** The `onlyoffice.server_name` variable needs to be overwritten with an
URL that points to your onlyoffice for this to work correctly.

| Parameter                                         | Description                                                   | Default                                                     |
| ------------------------------------------------- | ------------------------------------------------------------- | ----------------------------------------------------------- |
| `replicaCount`                                    | Number of provisioner instances to deployed                   | `1`                                                         |
| `strategy`                                        | Specifies the strategy used to replace old Pods by new ones   | `Recreate`                                                  |
| `image.repository`                                | Provisioner image                                             | `onlyoffice/documentserver`                                 |
| `image.tag`                                       | Version of provisioner image                                  | `5.3.1.26`                                                  |
| `image.pullPolicy`                                | Image pull policy                                             | `IfNotPresent`                                              |
| `onlyoffice.server_name`                          | Onlyoffice server URL                                         | `onlyoffice.domain`                                         |
| `ingress.enabled`                                 |                                                               | `false`                                                     |
| `ingress.annotations`                             |                                                               | `{}`                                                        |
| `ingress.paths`                                   |                                                               | `[]`                                                        |
| `ingress.hosts`                                   |                                                               | `[]`                                                        |
| `ingress.tls`                                     |                                                               | `[]`                                                        |
| `livenessProbe.enabled`                           | Turn on and off liveness probe                                | `true`                                                      |
| `livenessProbe.initialDelaySeconds`               | Delay before liveness probe is initiated                      | `30`                                                        |
| `livenessProbe.periodSeconds`                     | How often to perform the probe                                | `10`                                                        |
| `livenessProbe.timeoutSeconds`                    | When the probe times out                                      | `2`                                                         |
| `livenessProbe.successThreshold`                  | Minimum consecutive successes for the probe                   | `1`                                                         |
| `livenessProbe.failureThreshold`                  | Minimum consecutive failures for the probe                    | `3`                                                         |
| `readinessProbe.enabled`                          | Turn on and off readiness probe                               | `true`                                                      |
| `readinessProbe.initialDelaySeconds`              | Delay before readiness probe is initiated                     | `30`                                                        |
| `readinessProbe.periodSeconds`                    | How often to perform the probe                                | `10`                                                        |
| `readinessProbe.timeoutSeconds`                   | When the probe times out                                      | `2`                                                         |
| `readinessProbe.successThreshold`                 | Minimum consecutive successes for the probe                   | `1`                                                         |
| `readinessProbe.failureThreshold`                 | Minimum consecutive failures for the probe                    | `3`                                                         |
| `securityContext`                                 | Change pod security contenxt                                  | `{}`                                                        |
| `resources`                                       | Resources required (e.g. CPU, memory)                         | `{}`                                                        |
| `nodeSelector`                                    | Node labels for pod assignment                                | `{}`                                                        |
| `affinity`                                        | Affinity settings                                             | `{}`                                                        |
| `tolerations`                                     | List of node taints to tolerate                               | `[]`                                                        |
